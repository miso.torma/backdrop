import 'package:backdrop/backdrop.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  Widget _longBody(){
    List<Widget> body = [];
    for (int i = 0; i < 20; i++){
      body.add(ListTile(title: Text("Tile $i"),));
    }
    return ListView(children: body,);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Backdrop Demo',
      home: BackdropScaffold(
        title: Text("Backdrop Example"),
        backLayer: Center(
          child: Text("Back Layer"),
        ),
        frontLayer: Center(
          child: _longBody(),
        ),
        iconPosition: BackdropIconPosition.leading,
        actions: <Widget>[
          BackdropToggleButton(
            icon: AnimatedIcons.list_view,
          ),
        ],
        frontLayerBorderRadius: BorderRadius.only(topLeft: Radius.circular(200.0)),
        mainIcon: AnimatedIcons.add_event,
        enableRadiusOverflow: false,
      ),
    );
  }
}
